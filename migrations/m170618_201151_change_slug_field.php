<?php

use yii\db\Migration;

class m170618_201151_change_slug_field extends Migration {

    public function safeUp() {
        $this->execute('ALTER TABLE `products` Change `slug` `permalink` VARCHAR(255);');
    }

    public function safeDown() {
        echo "m170618_201151_change_slug_field cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m170618_201151_change_slug_field cannot be reverted.\n";

      return false;
      }
     */
}
