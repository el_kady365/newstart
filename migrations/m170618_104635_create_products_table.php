<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m170618_104635_create_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(255),
            'slug'=>$this->string(255),
            'description'=>$this->text(),
            'image'=>$this->string(255)->null(),
            'created'=>$this->dateTime()->null(),
            'updated'=>$this->dateTime()->null(),
        ]);
        
        $this->execute('ALTER TABLE `products` ADD `active` TINYINT(1) NULL AFTER `image`;');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('products');
    }
}
