<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $title
 * @property string $permalink
 * @property string $description
 * @property string $image
 * @property integer $active
 * @property string $created
 * @property string $updated 
 */
class Product extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public $upload;

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created','updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated',
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s'); // unix timestamp },
                },
            ]
        ];
    }

    public static function tableName() {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'permalink', 'description'], 'required'],
            [['active'], 'integer'],
            [['created','updated'], 'safe'],
            [['title', 'permalink'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'permalink' => 'Permalink',
            'description' => 'Description',
            'image' => 'Image',
            'active' => 'Active',
            'created' => 'Created',
            'updated'=>'Updated'
        ];
    }

}
